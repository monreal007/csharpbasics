﻿using System;

namespace CSharpBasics.Utilities
{
	public class Calculator
	{
        /// <summary>
        /// Вычисляет сумму всех натуральных чисел меньше <see cref="number"/>, кратных любому из <see cref="divisors"/>
        /// </summary>
        /// <param name="number">Натуральное число</param>
        /// <param name="divisors">Числа, которым должны быть кратны слагаемые искомой суммы</param>
        /// <returns>Вычисленная сумма</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Выбрасывается в случае, если <see cref="number"/> или любое из чисел в <see cref="divisors"/> не является натуральным,
        /// а также если массив <see cref="divisors"/> пустой
        /// </exception>
        public float CalcSumOfDivisors(int number, params int[] divisors)
        {
            if (divisors.Length == 0)        
                throw new ArgumentOutOfRangeException(nameof(divisors));

            if (number <= 0)
                throw new ArgumentOutOfRangeException(nameof(number));

            foreach (int divisor in divisors)
                if (divisor <= 0) throw new ArgumentOutOfRangeException(nameof(divisor));

            float result = default;
            for (int i = 1; i < number; i++)
            {
                foreach (int divisor in divisors)
                {
                    if (i % divisor == 0)
                    {
                        result += i;
                        break;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Возвращает ближайшее наибольшее целое, состоящее из цифр исходного числа <see cref="number"/>
        /// </summary>
        /// <param name="number">Исходное число</param>
        /// <returns>Ближайшее наибольшее целое</returns>
        public long FindNextBiggerNumber(int number)
		{
            if (number <= 0) throw new ArgumentOutOfRangeException(nameof(number));

            var numberInString = number.ToString();
            char[] numbersInChar = numberInString.ToCharArray();
            int lastNum = numberInString[numbersInChar.Length - 1];
            int j, i;
            for (i = numbersInChar.Length -1; i >= 0; i--)
            {

                if (lastNum > numbersInChar[i])
                {
                    break;
                }
                lastNum = numbersInChar[i];
            }
            if (i >= 0)
            {
                for(j = numbersInChar.Length - 1; j > i; j--)
                {
                    if (numbersInChar[j] > numbersInChar[i])
                    {
                        break;
                    }
                }
                Swap(numbersInChar, i, j);
                SortNum(numbersInChar, i + 1, numberInString.Length - 1);
            }
            else
            {
                throw new InvalidOperationException("The value does not exist");
            }

           return int.Parse(numbersInChar);

        }

        void Swap (char[] number, int i, int j)
        {
            char item = number[i];
            number[i] = number[j];
            number[j] = item;
        }

        void SortNum(char[] number, int i, int j)
        {
            while (i < j)
            {
                Swap(number, i, j);
                i += 1;
                j -= 1;
            }
        }
    }


}
	
