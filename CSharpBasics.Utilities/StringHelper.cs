﻿using System;

namespace CSharpBasics.Utilities
{
	public class StringHelper
	{
		/// <summary>
		/// Вычисляет среднюю длину слова в переданной строке без учётов знаков препинания и специальных символов
		/// </summary>
		/// <param name="inputString">Исходная строка</param>
		/// <returns>Средняя длина слова</returns>
		public int GetAverageWordLength(string inputString)
		{
			int result = 0;
			if (string.IsNullOrEmpty(inputString)) return result;

			string str = inputString + ' ';
			var results = new string[4];
			var currentWord = string.Empty;
			int wordCount = 0;
			int letterCount = 0;

			foreach (var letter in str)
			{
				if (!char.IsWhiteSpace(letter) && 
					!char.IsDigit(letter) &&
					!char.IsPunctuation(letter) &&
					!char.IsSeparator(letter) &&
					!char.IsSymbol(letter))
				{
					currentWord += letter;
					letterCount++;
				}
				else if (char.IsWhiteSpace(letter) && (currentWord == string.Empty))
				{
					continue;
				}
				else if (char.IsWhiteSpace(letter))
				{
					results[wordCount] = currentWord;
					currentWord = string.Empty;
					wordCount++;
				}
				
			}

			if (wordCount != 0) 
				result = letterCount / wordCount;
	   
			return result;
		}


		/// <summary>
		/// Удваивает в строке <see cref="original"/> все буквы, принадлежащие строке <see cref="toDuplicate"/>
		/// </summary>
		/// <param name="original">Строка, символы в которой нужно удвоить</param>
		/// <param name="toDuplicate">Строка, символы из которой нужно удвоить</param>
		/// <returns>Строка <see cref="original"/> с удвоенными символами, которые есть в строке <see cref="toDuplicate"/></returns>
		public string DuplicateCharsInString(string original, string toDuplicate)
		{
			if (string.IsNullOrEmpty(original)) return original;
			if (string.IsNullOrEmpty(toDuplicate)) return original;

			toDuplicate = toDuplicate.ToLower();
			string result = string.Empty;
			foreach (char letter in original)
			{
				char letterLower = char.ToLower(letter);
				if (toDuplicate.Contains(letterLower))
					result = result + letter + letter;
				else
				{
					result += letter;
				}	
			}
			return result;
		}
	}
}