﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpBasics.Utilities
{
	public class ArrayHelper
	{
		/// <summary>
		/// Вычисляет сумму неотрицательных элементов в одномерном массиве
		/// </summary>
		/// <param name="numbers">Одномерный массив чисел</param>
		/// <returns>Сумма неотрицательных элементов массива</returns>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfPositiveElements(int[] numbers)
		{
			float result = 0;
			if (numbers == null)
				throw new ArgumentNullException(nameof(numbers));

			foreach (int number in numbers)
				if (number >= 0)
					result += number;

			return result;
		}

		/// <summary>
		/// Заменяет все отрицательные элементы в трёхмерном массиве на нули
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public void ReplaceNegativeElementsBy0(int[,,] numbers)
		{
			if (numbers == null) throw new ArgumentNullException(nameof(numbers));

			for (int i = 0; i < numbers.GetLength(0); i++)
			{
				for (int j = 0; j < numbers.GetLength(1); j++)
				{
					for (int k = 0; k < numbers.GetLength(2); k++)
					{
						if (numbers[i, j, k] < 0) numbers[i, j, k] = 0;
					}

				}
			}

		}

		/// <summary>
		/// Вычисляет сумму элементов двумерного массива <see cref="numbers"/>,
		/// которые находятся на чётных позициях ([1,1], [2,4] и т.д.)
		/// </summary>
		/// <param name="numbers">Двумерный массив целых чисел</param>
		/// <returns>Сумма элементов на четных позициях</returns>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfElementsOnEvenPositions(int[,] numbers)
		{
			if (numbers == null) throw new ArgumentNullException(nameof(numbers));

			float result = 0;

			for (int i = 0; i < numbers.GetLength(0); i++)
			{
				for (int j = 0; j < numbers.GetLength(1); j++)
					if ((j + i) % 2 == 0)
						result += numbers[i, j];
			}

			return result;
		}

		/// <summary>
		/// Фильтрует массив <see cref="numbers"/> таким образом, чтобы на выходе остались только числа, содержащие цифру <see cref="filter"/>
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <param name="filter">Цифра для фильтрации массива <see cref="numbers"/></param>
		/// <returns></returns>
		public int[] FilterArrayByDigit(int[] numbers, byte filter)
		{
			if (numbers == null) return new int[0];

			var result = new List<int>();

			foreach (int num in numbers)
            {
				if (IsNumberContainsDigit(num, filter)) result.Add(num);
			}

			return result.ToArray();
		}

		bool IsNumberContainsDigit(int number, int digit)
        {
			do
			{
				if (number % 10 == digit) return true;

				number /= 10;
			}
			while (number > 0);

			return false;
        }
		 
	}
}